package com.meli.melifinder.business.server.service

import com.meli.melifinder.business.server.response.Description
import com.meli.melifinder.business.server.response.ProductDetailsResponse
import com.meli.melifinder.business.server.response.ProductListResponse
import com.meli.melifinder.common.Constants.GET_DESCRIPTION
import com.meli.melifinder.common.Constants.GET_DETAILS
import com.meli.melifinder.common.Constants.GET_PRODUCTS
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ProductApiService {

    @GET(GET_PRODUCTS)
    suspend fun getProductsByName(
        @Query("q") productName: String
    ): ProductListResponse

    @GET(GET_DETAILS)
    suspend fun getDetailsById(
        @Path("productId") productId: String
    ): ProductDetailsResponse

    @GET(GET_DESCRIPTION)
    suspend fun getDescriptionById(
        @Path("productId") productId: String
    ): Description

}