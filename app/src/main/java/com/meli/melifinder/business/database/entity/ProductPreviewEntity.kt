package com.meli.melifinder.business.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ProductPreview")
data class ProductPreviewEntity(
    @PrimaryKey
    var id: String,
    var acceptsMercadopago: Boolean,
    var location: String,
    var pictureUrl: String,
    var price: Int,
    var title: String
)