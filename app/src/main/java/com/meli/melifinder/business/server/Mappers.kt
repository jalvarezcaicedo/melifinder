package com.meli.melifinder.business.server

import com.meli.melifinder.business.server.response.Description
import com.meli.melifinder.business.server.response.ProductDetailsResponse
import com.meli.melifinder.business.server.response.ProductPreviewResponse
import com.meli.melifinder.domain.ProductDetails
import com.meli.melifinder.domain.ProductPreview


fun List<ProductPreviewResponse>.toDomain(): List<ProductPreview> = map { it.toProductsDomain() }

fun ProductPreviewResponse.toProductsDomain() = ProductPreview(
    acceptsMercadopago = acceptsMercadopago ?: false,
    id = id ?: "",
    location = (sellerAddress?.city?.name + ", " + sellerAddress?.state?.name),
    pictureUrl = thumbnail ?: "",
    price = price ?: 0,
    title = title ?: "",
)

fun ProductDetailsResponse.toProductDetailsDomain(
    description: Description
) = ProductDetails(
    acceptsMercadopago = acceptsMercadopago ?: false,
    availableQuantity = availableQuantity ?: 0,
    condition = condition ?: "",
    description = description.text ?: "",
    freeShipping = shipping?.freeShipping ?: false,
    id = id ?: "",
    location = (sellerAddress?.city?.name + ", " + sellerAddress?.state?.name),
    pictureUrl = secureThumbnail ?: "",
    picturesUrl = pictures?.mapNotNull { it?.secureUrl } ?: emptyList(),
    price = price ?: 0,
    title = title ?: ""
)