package com.meli.melifinder.business.database.source

import com.meli.melifinder.business.database.ProductDB
import com.meli.melifinder.business.database.toProductPreviewList
import com.meli.melifinder.business.database.toProductPreviewListEntity
import com.meli.melifinder.data.source.LocalProductDataSource
import com.meli.melifinder.domain.ProductPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class LocalProductDataSourceImpl(
    private val db: ProductDB
) : LocalProductDataSource {

    private val productPreviewDao by lazy { db.productPreviewDao() }

    override fun getLocalProductsData(): Flow<List<ProductPreview>> =
        productPreviewDao.findProductsPreview().map {
            it.toProductPreviewList()
        }

    override suspend fun saveProducts(products: List<ProductPreview>) =
        productPreviewDao.insertProductsPreview(products.toProductPreviewListEntity())

    override suspend fun removeProducts() {
        productPreviewDao.deleteProductsPreview()
    }
}