package com.meli.melifinder.business.server.response

import com.google.gson.annotations.SerializedName

data class ProductListResponse(
    val results: List<ProductPreviewResponse>
)

data class ProductPreviewResponse(
    @SerializedName("accepts_mercadopago")
    val acceptsMercadopago: Boolean?,
    val id: String?,
    @SerializedName("seller_address")
    val sellerAddress: SellerAddress?,
    val price: Int?,
    val title: String?,
    val thumbnail: String?,
)

data class SellerAddress(
    val city: City?,
    val state: State?
)

data class State(
    val id: String,
    val name: String
)

data class City(
    val id: String,
    val name: String
)