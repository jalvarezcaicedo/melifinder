package com.meli.melifinder.business.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.meli.melifinder.business.database.entity.ProductPreviewEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ProductPreviewDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProductsPreview(productsPreview: List<ProductPreviewEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProductPreview(productPreviewEntity: ProductPreviewEntity)

    @Query("SELECT * FROM ProductPreview")
    fun findProductsPreview(): Flow<List<ProductPreviewEntity>>

    @Query("DELETE FROM ProductPreview")
    fun deleteProductsPreview()

}