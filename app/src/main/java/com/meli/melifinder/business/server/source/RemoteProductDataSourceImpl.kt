package com.meli.melifinder.business.server.source

import arrow.core.Either
import com.meli.melifinder.business.server.response.Description
import com.meli.melifinder.business.server.response.ProductDetailsResponse
import com.meli.melifinder.business.server.service.ProductApiService
import com.meli.melifinder.business.server.toDomain
import com.meli.melifinder.business.server.toProductDetailsDomain
import com.meli.melifinder.business.util.tryCall
import com.meli.melifinder.data.source.RemoteProductDataSource
import com.meli.melifinder.domain.Error
import com.meli.melifinder.domain.ProductDetails
import com.meli.melifinder.domain.ProductPreview
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

class RemoteProductDataSourceImpl(
    private val productApiService: ProductApiService
) : RemoteProductDataSource {

    override suspend fun getProductsByName(name: String): Either<Error, List<ProductPreview>> =
        tryCall {
            productApiService.getProductsByName(name).results.toDomain()
        }

    override suspend fun getProductDetailsById(productId: String): Either<Error, ProductDetails> =
        tryCall {

            lateinit var details: ProductDetailsResponse
            lateinit var description: Description

            coroutineScope {
                val detailsResponse = async { productApiService.getDetailsById(productId) }
                val descriptionResponse = async { productApiService.getDescriptionById(productId) }

                details = detailsResponse.await()
                description = descriptionResponse.await()
            }

            details.toProductDetailsDomain(description)
        }

}