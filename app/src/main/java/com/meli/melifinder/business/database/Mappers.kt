package com.meli.melifinder.business.database

import com.meli.melifinder.business.database.entity.ProductPreviewEntity
import com.meli.melifinder.domain.ProductPreview

fun ProductPreview.toProductPreviewEntity() = ProductPreviewEntity(
    id, acceptsMercadopago, location, pictureUrl, price, title
)

fun ProductPreviewEntity.toProductPreview() = ProductPreview(
    acceptsMercadopago, id, location, pictureUrl, price, title
)

fun List<ProductPreviewEntity>.toProductPreviewList(): List<ProductPreview> = map {
    it.toProductPreview()
}

fun List<ProductPreview>.toProductPreviewListEntity(): List<ProductPreviewEntity> = map {
    it.toProductPreviewEntity()
}