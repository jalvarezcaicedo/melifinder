package com.meli.melifinder.business.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.meli.melifinder.business.database.dao.ProductPreviewDao
import com.meli.melifinder.business.database.entity.ProductPreviewEntity

@Database(
    entities = [
        ProductPreviewEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class ProductDB : RoomDatabase() {

    abstract fun productPreviewDao(): ProductPreviewDao

    companion object {
        @Synchronized
        fun getDatabase(context: Context): ProductDB = Room.databaseBuilder(
            context.applicationContext, ProductDB::class.java, "products_db"
        ).build()
    }


}