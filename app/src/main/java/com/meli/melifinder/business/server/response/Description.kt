package com.meli.melifinder.business.server.response

import com.google.gson.annotations.SerializedName

data class Description(
    @SerializedName("plain_text")
    val text: String?
)