package com.meli.melifinder.modules

import com.meli.melifinder.business.server.service.ProductApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

@Module
@InstallIn(SingletonComponent::class)
class Services {

    @Provides
    fun provideProductsApiService(retrofit: Retrofit): ProductApiService =
        retrofit.create(ProductApiService::class.java)

}