package com.meli.melifinder.modules

import android.app.Application
import com.meli.melifinder.business.database.ProductDB
import com.meli.melifinder.business.database.source.LocalProductDataSourceImpl
import com.meli.melifinder.business.server.service.ProductApiService
import com.meli.melifinder.business.server.source.RemoteProductDataSourceImpl
import com.meli.melifinder.data.repository.ProductDetailsRepository
import com.meli.melifinder.data.repository.ProductRepository
import com.meli.melifinder.data.source.LocalProductDataSource
import com.meli.melifinder.data.source.RemoteProductDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class Data {

    @Provides
    @Singleton
    fun databaseProvider(app: Application): ProductDB = ProductDB.getDatabase(app)

    @Provides
    fun productRepository(
        localProductDataSource: LocalProductDataSource,
        remoteProductDataSource: RemoteProductDataSource
    ) = ProductRepository(localProductDataSource, remoteProductDataSource)

    @Provides
    fun productDetailsRepository(
        remoteProductDataSource: RemoteProductDataSource
    ) = ProductDetailsRepository(remoteProductDataSource)

    @Provides
    fun productLocalDataSourceProvider(
        db: ProductDB
    ): LocalProductDataSource = LocalProductDataSourceImpl(db)

    @Provides
    fun productRemoteDataSourceProvider(
        productApiService: ProductApiService
    ): RemoteProductDataSource = RemoteProductDataSourceImpl(productApiService)


}