package com.meli.melifinder.common

object Constants {
    private const val SITE_ID = "MCO"

    //ENDPOINTS
    const val GET_PRODUCTS = "sites/$SITE_ID/search"
    const val GET_DETAILS = "items/{productId}"
    const val GET_DESCRIPTION = "items/{productId}/description"
    const val EMPTY_STRING = ""
}