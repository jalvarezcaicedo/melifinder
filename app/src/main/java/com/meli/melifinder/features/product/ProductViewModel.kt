package com.meli.melifinder.features.product

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.meli.melifinder.domain.Error
import com.meli.melifinder.domain.ProductPreview
import com.meli.melifinder.usecase.GetLocalProductsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject constructor(
    private val getLocalProductsUseCase: GetLocalProductsUseCase
) : ViewModel() {

    private val _state = MutableStateFlow(UIState())
    val state: StateFlow<UIState> = _state.asStateFlow()

    fun getLocalProducts() = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getLocalProductsUseCase().catch {
            _state.update { UIState(error = it.error, loading = false) }
        }.collect { products ->
            _state.update { UIState(products = products, loading = false) }
        }
    }

    data class UIState(
        val error: Error? = null,
        val loading: Boolean = false,
        val products: List<ProductPreview>? = null
    )

}