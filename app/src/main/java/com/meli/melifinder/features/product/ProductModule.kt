package com.meli.melifinder.features.product

import com.meli.melifinder.data.repository.ProductRepository
import com.meli.melifinder.usecase.GetLocalProductsUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
class ProductModule {

    @Provides
    @ViewModelScoped
    fun getLocalProductsUseCase(
        productsRepository: ProductRepository
    ) = GetLocalProductsUseCase(productsRepository)

}