package com.meli.melifinder.features.search

import com.meli.melifinder.data.repository.ProductRepository
import com.meli.melifinder.usecase.GetRemoteProductsUseCase
import com.meli.melifinder.usecase.RemoveProductsUseCase
import com.meli.melifinder.usecase.SaveProductUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
class SearchModule {

    @Provides
    @ViewModelScoped
    fun saveProductUseCase(
        productsRepository: ProductRepository
    ) = SaveProductUseCase(productsRepository)

    @Provides
    @ViewModelScoped
    fun removeProductsUseCase(
        productsRepository: ProductRepository
    ) = RemoveProductsUseCase(productsRepository)

    @Provides
    @ViewModelScoped
    fun getRemoteProductsUseCase(
        productsRepository: ProductRepository
    ) = GetRemoteProductsUseCase(productsRepository)

}