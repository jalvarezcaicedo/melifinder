package com.meli.melifinder.features.details

import com.meli.melifinder.data.repository.ProductRepository
import com.meli.melifinder.usecase.GetRemoteProductDetailsUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
class ProductDetailsModule {

    @Provides
    @ViewModelScoped
    fun getRemoteProductDetailsUseCase(
        productsRepository: ProductRepository
    ) = GetRemoteProductDetailsUseCase(productsRepository)

}