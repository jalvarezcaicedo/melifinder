package com.meli.melifinder.features.details

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.meli.melifinder.R
import com.meli.melifinder.common.launchAndCollect
import com.meli.melifinder.databinding.FragmentProductDetailsBinding
import com.meli.melifinder.domain.ProductPreview
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import java.text.NumberFormat
import java.util.Locale

@AndroidEntryPoint
class ProductDetailsFragment : Fragment(R.layout.fragment_product_details) {

    private lateinit var binding: FragmentProductDetailsBinding
    private val productDetailsViewModel: ProductDetailsViewModel by viewModels()
    private val safeArgs: ProductDetailsFragmentArgs by navArgs()
    private var productId: String = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentProductDetailsBinding.bind(view)
        getParametersFragment()
        initObservables()
        productDetailsViewModel.getProductDetailsDataServer(productId)
    }

    private fun getParametersFragment() {
        productId = safeArgs.productId
    }

    private fun initObservables() {
        with(productDetailsViewModel.state) {
            diff({ it.loading }) { handleVisibility(it, listOf()) }
            diff({ it.productDetails }) {
                it?.let {
                    //set data into UI
                    binding.tvTitle.text = it.title
                    binding.tvFreeShipping.visibility =
                        if (it.freeShipping) View.VISIBLE else View.GONE
                    binding.tvProductState.text = it.condition
                    binding.tvPrice.text =
                        NumberFormat.getCurrencyInstance(
                            Locale("es", "CO")
                        ).format(it.price)

                    binding.tvAvailableProducts.text =
                        String.format(resources.getString(R.string.quantity), it.availableQuantity)
                    binding.tvLocation.text = it.location
                    binding.tvDescription.text = it.description

                    val carouselItems: MutableList<String> = mutableListOf()

                    it.picturesUrl.forEach { pictureUrl ->
                        carouselItems.add(pictureUrl)
                    }

                    binding.vpCarousel.adapter =
                        ImageSliderPagerAdapter(requireContext(), carouselItems)

                }
            }
            diff({ it.error }) {
                it?.let {
                    com.google.android.material.snackbar.Snackbar.make(
                        binding.clProductDetails,
                        getString(R.string.error_product_details),
                        com.google.android.material.snackbar.Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun handleVisibility(loading: Boolean, data: List<ProductPreview>) {
        Log.d("Visibility", loading.toString().plus(" $data"))
    }

    private fun <T, U> Flow<T>.diff(mapFlow: (T) -> U, body: (U) -> Unit) {
        viewLifecycleOwner.launchAndCollect(
            flow = map(mapFlow).distinctUntilChanged(), body = body
        )
    }
}