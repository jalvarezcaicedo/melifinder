package com.meli.melifinder.features.search

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.meli.melifinder.R
import com.meli.melifinder.common.Constants.EMPTY_STRING
import com.meli.melifinder.common.hideKeyboard
import com.meli.melifinder.common.launchAndCollect
import com.meli.melifinder.databinding.FragmentSearchBinding
import com.meli.melifinder.domain.ProductPreview
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import java.util.Timer
import java.util.TimerTask

@AndroidEntryPoint
class SearchProductFragment : Fragment(R.layout.fragment_search) {

    lateinit var binding: FragmentSearchBinding
    private val searchViewModel: SearchViewModel by viewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSearchBinding.bind(view)
        searchViewModel.removeProductsPreview()
        searchViewModel.removeRemoteProductsPreview()
        initListener()
        initObservables()
    }

    private fun initListener() {

        binding.etFinder.editText?.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchProduct()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
        binding.etFinder.editText?.addTextChangedListener(object : TextWatcher {
            private var timer = Timer()
            private val DELAY: Long = 3000L

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit

            override fun afterTextChanged(p0: Editable?) {
                timer.cancel()
                timer = Timer()
                timer.schedule(object : TimerTask() {
                    override fun run() {
                        binding.searcherTxt.text.takeUnless { it.isNullOrEmpty() }
                            ?.let { searchProduct() } ?: timer.cancel()
                    }
                }, DELAY)
            }
        })
    }

    private fun initObservables() {
        with(searchViewModel.state) {
            diff({ it.loading }) { handleVisibility(it, listOf()) }
            diff({ it.remoteProducts }) {
                it?.let {
                    if (it.isNotEmpty()) {
                        searchViewModel.saveProductsPreview(it)
                        requireActivity().runOnUiThread {
                            findNavController().navigate(R.id.action_searchProductFragment_to_productFragment)
                        }
                    }
                }
            }
            diff({ it.error }) {
                it?.let {
                    Snackbar.make(binding.clSearch, it.toString(), Snackbar.LENGTH_SHORT).show()
                    clear()
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        clear()
    }

    private fun searchProduct() {
        hideKeyboard()
        searchViewModel.getProductsDataServer(binding.searcherTxt.text.toString())
    }

    private fun clear() {
        binding.searcherTxt.setText(EMPTY_STRING)
    }

    private fun handleVisibility(loading: Boolean, data: List<ProductPreview>) {
        Log.d("Visibility", loading.toString().plus(" $data"))
    }

    private fun <T, U> Flow<T>.diff(mapFlow: (T) -> U, body: (U) -> Unit) {
        viewLifecycleOwner.launchAndCollect(
            flow = map(mapFlow).distinctUntilChanged(), body = body
        )
    }
}