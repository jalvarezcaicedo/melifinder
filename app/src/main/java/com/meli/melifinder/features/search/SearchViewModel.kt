package com.meli.melifinder.features.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.meli.melifinder.domain.Error
import com.meli.melifinder.domain.ProductPreview
import com.meli.melifinder.usecase.GetRemoteProductsUseCase
import com.meli.melifinder.usecase.RemoveProductsUseCase
import com.meli.melifinder.usecase.SaveProductUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val saveProductUseCase: SaveProductUseCase,
    private val removeProductsUseCase: RemoveProductsUseCase,
    private val getRemoteProductsUseCase: GetRemoteProductsUseCase
) : ViewModel() {

    private val _state = MutableStateFlow(UIState())
    val state: StateFlow<UIState> = _state.asStateFlow()

    fun saveProductsPreview(products: List<ProductPreview>) = viewModelScope.launch {
        saveProductUseCase.invoke(products)
    }

    fun removeProductsPreview() = viewModelScope.launch(Dispatchers.IO) {
        removeProductsUseCase.invoke()
    }

    fun removeRemoteProductsPreview() = viewModelScope.launch {
        _state.update { it.copy(remoteProducts = emptyList()) }
    }

    fun getProductsDataServer(name: String) = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getRemoteProductsUseCase(name).fold(
            ifLeft = { error -> _state.update { it.copy(error = error, loading = false) } },
            ifRight = { response ->
                _state.update {
                    it.copy(
                        remoteProducts = response,
                        loading = false
                    )
                }
            }
        )
    }

    data class UIState(
        val error: Error? = null,
        val loading: Boolean = false,
        val remoteProducts: List<ProductPreview>? = null
    )

}