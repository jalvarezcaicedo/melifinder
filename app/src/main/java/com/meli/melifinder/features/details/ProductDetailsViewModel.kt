package com.meli.melifinder.features.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.meli.melifinder.domain.Error
import com.meli.melifinder.domain.ProductDetails
import com.meli.melifinder.usecase.GetRemoteProductDetailsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductDetailsViewModel @Inject constructor(
    private val getRemoteProductDetailsUseCase: GetRemoteProductDetailsUseCase
) : ViewModel() {

    private val _state = MutableStateFlow(UIState())
    val state: StateFlow<UIState> = _state.asStateFlow()

    fun getProductDetailsDataServer(productId: String) = viewModelScope.launch {
        _state.update { UIState(loading = true) }
        getRemoteProductDetailsUseCase(productId).fold(
            ifLeft = { error -> _state.update { it.copy(error = error, loading = false) } },
            ifRight = { response ->
                _state.update {
                    it.copy(
                        productDetails = response,
                        loading = false
                    )
                }
            }
        )
    }

    data class UIState(
        val error: Error? = null,
        val loading: Boolean = false,
        val productDetails: ProductDetails? = null
    )

}