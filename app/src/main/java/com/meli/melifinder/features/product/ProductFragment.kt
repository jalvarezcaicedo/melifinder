package com.meli.melifinder.features.product

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.meli.melifinder.R
import com.meli.melifinder.common.launchAndCollect
import com.meli.melifinder.databinding.FragmentProductPreviewBinding
import com.meli.melifinder.domain.ProductPreview
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map

@AndroidEntryPoint
class ProductFragment : Fragment(R.layout.fragment_product_preview),
    ProductAdapter.OnProductClickListener {

    private lateinit var binding: FragmentProductPreviewBinding
    private val productViewModel: ProductViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentProductPreviewBinding.bind(view)
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ProductFragment.requireContext())
        }
        initObservables()
        productViewModel.getLocalProducts()
    }

    private fun initObservables() {
        with(productViewModel.state) {
            diff({ it.loading }) { handleVisibility(it, listOf()) }
            diff({ it.products }) {
                it?.let {
                    binding.recyclerView.addItemDecoration(
                        DividerItemDecoration(
                            requireContext(),
                            DividerItemDecoration.VERTICAL
                        )
                    )

                    binding.recyclerView.adapter = ProductAdapter(it, this@ProductFragment)
                }
            }
        }
    }

    override fun onProductClick(productPreview: ProductPreview) {
        val action = ProductFragmentDirections.actionProductFragmentToDetailProductFragment(
            productPreview.id
        )
        findNavController().navigate(action)
    }

    private fun handleVisibility(loading: Boolean, data: List<ProductPreview>) {
        Log.d("Visibility", loading.toString().plus(" $data"))
        binding.progressBar.isVisible = loading
    }

    private fun <T, U> Flow<T>.diff(mapFlow: (T) -> U, body: (U) -> Unit) {
        viewLifecycleOwner.launchAndCollect(
            flow = map(mapFlow).distinctUntilChanged(), body = body
        )
    }

}