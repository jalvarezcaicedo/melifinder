package com.meli.melifinder.features.product

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.meli.melifinder.common.BaseViewHolder
import com.meli.melifinder.databinding.ItemProductPreviewBinding
import com.meli.melifinder.domain.ProductPreview

class ProductAdapter(
    private var productList: List<ProductPreview>,
    private val itemClickListener: OnProductClickListener
) : RecyclerView.Adapter<BaseViewHolder<*>>() {

    interface OnProductClickListener {
        fun onProductClick(productPreview: ProductPreview)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val itemBinding = ItemProductPreviewBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        val holder = ProductViewHolder(itemBinding, parent.context)

        itemBinding.root.setOnClickListener {
            val position =
                holder.bindingAdapterPosition.takeIf { it != DiffUtil.DiffResult.NO_POSITION }
                    ?: return@setOnClickListener
            itemClickListener.onProductClick(productList[position])
        }
        return holder
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is ProductViewHolder -> holder.bind(productList[position])
        }
    }

    override fun getItemCount(): Int = productList.size


    private inner class ProductViewHolder(
        val binding: ItemProductPreviewBinding, val context: Context
    ) : BaseViewHolder<ProductPreview>(binding.root) {
        @SuppressLint("SetTextI18n")
        override fun bind(item: ProductPreview) {

            itemView.setOnClickListener { itemClickListener.onProductClick(item) }

            Glide.with(context).load(item.pictureUrl)
                .placeholder(android.R.drawable.ic_menu_gallery).centerCrop()
                .into(binding.imagProduct)
            binding.tvTitle.text = item.title
            binding.txPrecio.text = "$${item.price}"
        }
    }

}