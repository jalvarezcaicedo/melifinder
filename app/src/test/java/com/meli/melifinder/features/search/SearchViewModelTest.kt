package com.meli.melifinder.features.search

import arrow.core.right
import com.meli.melifinder.CoroutinesTestRule
import com.meli.melifinder.testshared.mockProductList
import com.meli.melifinder.usecase.GetRemoteProductsUseCase
import com.meli.melifinder.usecase.RemoveProductsUseCase
import com.meli.melifinder.usecase.SaveProductUseCase
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runCurrent
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SearchViewModelTest {

    @get:Rule
    val coroutinesTestRule = CoroutinesTestRule()

    @Mock
    lateinit var getRemoteProductsUseCase: GetRemoteProductsUseCase

    @Mock
    lateinit var saveProductUseCase: SaveProductUseCase

    @Mock
    lateinit var removeProductsUseCase: RemoveProductsUseCase

    private lateinit var searchViewModel: SearchViewModel

    @Before
    fun setup() {
        searchViewModel = SearchViewModel(
            saveProductUseCase,
            removeProductsUseCase,
            getRemoteProductsUseCase
        )
    }

    @Test
    fun `validate get remote products use case`() = runTest {
        whenever(getRemoteProductsUseCase("test")).thenReturn(mockProductList.right())
        searchViewModel.getProductsDataServer("test")

        val results = mutableListOf<SearchViewModel.UIState>()
        val job = launch { searchViewModel.state.toList(results) }
        runCurrent()
        job.cancel()

        assertEquals(
            SearchViewModel.UIState(remoteProducts = mockProductList), results[0]
        )
    }


}