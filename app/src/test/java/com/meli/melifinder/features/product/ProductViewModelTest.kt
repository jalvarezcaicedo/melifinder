package com.meli.melifinder.features.product

import com.meli.melifinder.CoroutinesTestRule
import com.meli.melifinder.testshared.mockProductList
import com.meli.melifinder.usecase.GetLocalProductsUseCase
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runCurrent
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ProductViewModelTest {

    @get:Rule
    val coroutinesTestRule = CoroutinesTestRule()

    @Mock
    lateinit var getLocalProductsUseCase: GetLocalProductsUseCase

    private lateinit var productViewModel: ProductViewModel

    @Before
    fun setUp() {
        productViewModel = ProductViewModel(
            getLocalProductsUseCase
        )
    }

    @Test
    fun `validate get local products use case`() = runTest {
        whenever(getLocalProductsUseCase()).thenReturn(flowOf(mockProductList))
        productViewModel.getLocalProducts()

        val results = mutableListOf<ProductViewModel.UIState>()
        val job = launch { productViewModel.state.toList(results) }
        runCurrent()
        job.cancel()

        TestCase.assertEquals(
            ProductViewModel.UIState(products = mockProductList), results[0]
        )
    }
}