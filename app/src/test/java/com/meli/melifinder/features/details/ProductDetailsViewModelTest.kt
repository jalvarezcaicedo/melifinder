package com.meli.melifinder.features.details

import arrow.core.right
import com.meli.melifinder.CoroutinesTestRule
import com.meli.melifinder.testshared.mockProductDetails
import com.meli.melifinder.usecase.GetRemoteProductDetailsUseCase
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runCurrent
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ProductDetailsViewModelTest {
    @get:Rule
    val coroutinesTestRule = CoroutinesTestRule()

    @Mock
    lateinit var getRemoteProductDetailsUseCase: GetRemoteProductDetailsUseCase

    lateinit var productDetailViewModel: ProductDetailsViewModel

    @Before
    fun setUp() {
        productDetailViewModel = ProductDetailsViewModel(
            getRemoteProductDetailsUseCase
        )
    }

    @Test
    fun `validate get remote product details use case`() = runTest {
        whenever(getRemoteProductDetailsUseCase("0000001")).thenReturn(mockProductDetails.right())
        productDetailViewModel.getProductDetailsDataServer("0000001")

        val results = mutableListOf<ProductDetailsViewModel.UIState>()
        val job = launch { productDetailViewModel.state.toList(results) }
        runCurrent()
        job.cancel()

        TestCase.assertEquals(
            ProductDetailsViewModel.UIState(productDetails = mockProductDetails), results[0]
        )
    }

}