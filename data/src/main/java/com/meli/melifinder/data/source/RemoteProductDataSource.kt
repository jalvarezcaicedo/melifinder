package com.meli.melifinder.data.source

import arrow.core.Either
import com.meli.melifinder.domain.Error
import com.meli.melifinder.domain.ProductDetails
import com.meli.melifinder.domain.ProductPreview

interface RemoteProductDataSource {

    suspend fun getProductsByName(name: String): Either<Error, List<ProductPreview>>

    suspend fun getProductDetailsById(productId: String): Either<Error, ProductDetails>

}