package com.meli.melifinder.data.repository

import arrow.core.Either
import com.meli.melifinder.data.source.LocalProductDataSource
import com.meli.melifinder.data.source.RemoteProductDataSource
import com.meli.melifinder.domain.Error
import com.meli.melifinder.domain.ProductDetails
import com.meli.melifinder.domain.ProductPreview
import kotlinx.coroutines.flow.Flow

class ProductRepository(
    private val localProductsDataSource: LocalProductDataSource,
    private val remoteDataSource: RemoteProductDataSource
) {

    suspend fun saveProductsData(products: List<ProductPreview>) =
        localProductsDataSource.saveProducts(products)

    suspend fun deleteProductsData() =
        localProductsDataSource.removeProducts()

    suspend fun getProductsByName(name: String): Either<Error, List<ProductPreview>> =
        remoteDataSource.getProductsByName(name)

    suspend fun getProductDetailsById(productId: String): Either<Error, ProductDetails> =
        remoteDataSource.getProductDetailsById(productId)

    fun getLocalProducts(): Flow<List<ProductPreview>> =
        localProductsDataSource.getLocalProductsData()

}