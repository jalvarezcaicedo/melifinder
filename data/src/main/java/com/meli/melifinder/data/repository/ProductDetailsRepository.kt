package com.meli.melifinder.data.repository

import com.meli.melifinder.data.source.RemoteProductDataSource

class ProductDetailsRepository(
    private val remoteDataSource: RemoteProductDataSource
) {

    suspend fun getProductDetails(productId: String) =
        remoteDataSource.getProductDetailsById(productId)

}