package com.meli.melifinder.data.source

import com.meli.melifinder.domain.ProductPreview
import kotlinx.coroutines.flow.Flow

interface LocalProductDataSource {

    fun getLocalProductsData(): Flow<List<ProductPreview>>

    suspend fun saveProducts(products: List<ProductPreview>)

    suspend fun removeProducts()

}