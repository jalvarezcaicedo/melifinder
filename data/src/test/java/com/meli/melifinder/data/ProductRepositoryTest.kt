package com.meli.melifinder.data

import arrow.core.Either
import arrow.core.right
import com.meli.melifinder.data.repository.ProductRepository
import com.meli.melifinder.data.source.LocalProductDataSource
import com.meli.melifinder.data.source.RemoteProductDataSource
import com.meli.melifinder.domain.ProductDetails
import com.meli.melifinder.domain.ProductPreview
import com.meli.melifinder.testshared.mockProductDetails
import com.meli.melifinder.testshared.mockProductList
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.whenever

@RunWith(MockitoJUnitRunner::class)
class ProductRepositoryTest {
    @Mock
    lateinit var remoteDataSource: RemoteProductDataSource

    @Mock
    lateinit var localDataSource: LocalProductDataSource

    private lateinit var productsRepository: ProductRepository

    @Before
    fun setUp() {
        productsRepository = ProductRepository(localDataSource, remoteDataSource)
    }

    @Test
    fun `when list of products by name success, return product list from server`() = runBlocking {
        val expectedResult: Either<Error, List<ProductPreview>> = Either.Right(mockProductList)
        whenever(remoteDataSource.getProductsByName("Product"))
            .thenReturn(mockProductList.right())

        val result = productsRepository.getProductsByName("Product")

        assertEquals(expectedResult, result)
    }

    @Test
    fun `when get details of product by id success, return details from server`() = runBlocking {
        val expectedResult: Either<Error, ProductDetails> = Either.Right(mockProductDetails)
        whenever(remoteDataSource.getProductDetailsById("id"))
            .thenReturn(mockProductDetails.right())

        val result = productsRepository.getProductDetailsById("id")

        assertEquals(expectedResult, result)
    }
}