package com.meli.melifinder.domain

data class ProductPreview(
    val acceptsMercadopago: Boolean,
    val id: String,
    val location: String,
    val pictureUrl: String,
    val price: Int,
    val title: String
)