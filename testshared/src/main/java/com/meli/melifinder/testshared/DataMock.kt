package com.meli.melifinder.testshared

import com.meli.melifinder.domain.ProductDetails
import com.meli.melifinder.domain.ProductPreview

val mockProductList = listOf(
    ProductPreview(
        false,
        "M0000001",
        "Medellin, Antioquia",
        "www.picture.com/M0000001.jpg",
        20000,
        "Product 1"
    ), ProductPreview(
        true,
        "M0000003",
        "Barranquilla, Atlantico",
        "www.picture.com/M0000002.jpg",
        3685686,
        "Product 2"
    )
)

val mockProductDetails = ProductDetails(
    false,
    7,
    "new",
    "This is a product description",
    true,
    "M0000005",
    "Barranquilla, Atlantico",
    "www.picture.com/M0000001.jpg",
    listOf("www.picture.com/M0000001.jpg", "www.picture.com/M0000001.jpg"),
    20000,
    "Product 1"
)