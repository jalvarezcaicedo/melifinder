package com.meli.melifinder.usecase

import arrow.core.Either
import com.meli.melifinder.data.repository.ProductRepository
import com.meli.melifinder.domain.Error
import com.meli.melifinder.domain.ProductPreview

class GetRemoteProductsUseCase(
    private val productsRepository: ProductRepository
) {
    suspend operator fun invoke(name: String): Either<Error, List<ProductPreview>> =
        productsRepository.getProductsByName(name)
}