package com.meli.melifinder.usecase

import arrow.core.Either
import com.meli.melifinder.data.repository.ProductRepository
import com.meli.melifinder.domain.Error
import com.meli.melifinder.domain.ProductDetails

class GetRemoteProductDetailsUseCase(
    private val productsRepository: ProductRepository
) {
    suspend operator fun invoke(productId: String): Either<Error, ProductDetails> =
        productsRepository.getProductDetailsById(productId)
}