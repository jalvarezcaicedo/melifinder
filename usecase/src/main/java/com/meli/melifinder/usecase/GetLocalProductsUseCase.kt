package com.meli.melifinder.usecase

import com.meli.melifinder.data.repository.ProductRepository
import com.meli.melifinder.domain.ProductPreview
import kotlinx.coroutines.flow.Flow

class GetLocalProductsUseCase(
    private val productRepository: ProductRepository
) {
    operator fun invoke(): Flow<List<ProductPreview>> = productRepository.getLocalProducts()
}