package com.meli.melifinder.usecase

import com.meli.melifinder.data.repository.ProductRepository

class RemoveProductsUseCase(
    private val productsRepository: ProductRepository
) {
    suspend operator fun invoke() = productsRepository.deleteProductsData()
}