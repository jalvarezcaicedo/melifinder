package com.meli.melifinder.usecase

import com.meli.melifinder.data.repository.ProductRepository
import com.meli.melifinder.domain.ProductPreview

class SaveProductUseCase(
    private val productsRepository: ProductRepository
) {
    suspend operator fun invoke(products: List<ProductPreview>) =
        productsRepository.saveProductsData(products)
}